package com.example.cmsc436_final_lost_and_found

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso

class ShowMap : AppCompatActivity() {

    companion object {
        const val TAG = "MapLocation"
    }

    // UI elements
    private lateinit var locationText: TextView
    private lateinit var buttonMap: Button
    private lateinit var buttonClaim: Button
    private lateinit var nameView: TextView
    private lateinit var categoryView: TextView
    private lateinit var descriptionView: TextView
    private lateinit var imageView: ImageView
    private lateinit var mStorageRef: StorageReference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Set content view
        setContentView(R.layout.search_result)


        mStorageRef = FirebaseStorage.getInstance().getReference("uploads");
        // Initialize UI elements
        locationText = findViewById(R.id.res_location)
        buttonMap = findViewById(R.id.button_map)
        nameView = findViewById(R.id.res_name)
        categoryView = findViewById(R.id.res_category)
        descriptionView = findViewById(R.id.res_description)
        imageView = findViewById(R.id.image_view)

        locationText.text = intent.getStringExtra("location")
        nameView.text = intent.getStringExtra("name")
        descriptionView.text = intent.getStringExtra("description")
        categoryView.text = intent.getStringExtra("category")
        if (intent.getStringExtra("imageUrl") !== null && !intent.getStringExtra("imageUrl").isEmpty())
        {
            Picasso.get()
                .load(intent.getStringExtra("imageUrl"))
                .into(imageView)
        }

        // Link UI elements to actions in code
        buttonMap.setOnClickListener { processClick() }

        buttonClaim =  findViewById(R.id.button_claim)

        buttonClaim.setOnClickListener{ makePhoneCall(intent.getStringExtra("phoneNum")) }
    }

    //Make phone call
    private fun makePhoneCall(number: String) : Boolean {

        try {
            val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$number"))
            startActivity(intent)
            return true
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
    }

    // Called when user clicks the Show Map button

    private fun processClick() {
        try {

            // Process text for network transmission
            var location = locationText.text.toString()
            location = location.replace(' ', '+')

            // Create Intent object for starting Google Maps application
            val geoIntent = Intent(
                Intent.ACTION_VIEW, Uri
                    .parse("geo:0,0?q=$location"))

            if (packageManager.resolveActivity(geoIntent, 0) != null) {
                // Use the Intent to start Google Maps application using Activity.startActivity()
                startActivity(geoIntent)
            }

        } catch (e: Exception) {
            // Log any error messages to LogCat using Log.e()
            Log.e(TAG, e.toString())
        }
    }

}
