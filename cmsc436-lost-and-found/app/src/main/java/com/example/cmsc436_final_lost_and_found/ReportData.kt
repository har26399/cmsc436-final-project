package com.example.cmsc436_final_lost_and_found

import java.util.*

data class ReportData (
    val id: String = "",
    val reporter: String = "",
    val name: String = "",
    val category: String = "",
    val location: String = "",
    val description: String = "",
    val imageUrl: String? = null,
    val phoneNumber: String = ""
)
