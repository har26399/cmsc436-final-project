package com.example.cmsc436_final_lost_and_found

import android.annotation.TargetApi
import android.content.Intent
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.Button
import android.widget.EditText
import android.widget.ListView
import android.widget.Spinner
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

import java.util.ArrayList
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.net.Uri
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.os.Build
import android.widget.*
import com.google.firebase.storage.StorageReference
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageTask
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.webkit.MimeTypeMap
import android.content.ContentResolver
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.os.Handler
import androidx.annotation.NonNull
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.storage.UploadTask


class ReportFound : AppCompatActivity() {

    lateinit var editTextName: EditText
    lateinit var spinnerCategory: Spinner
    lateinit var editTextLocation: EditText
    lateinit var editTextDescription: EditText
    lateinit var sendButton: Button
    lateinit var editTextPhone: EditText

    lateinit var uploadButton: Button

    lateinit var databaseReportFound: DatabaseReference
    lateinit var mStorageRef: StorageReference

    var imageUri: Uri? = null

    lateinit var imageView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.report_lost_object)

        val userMail = intent.getStringExtra("UserEmail")

        databaseReportFound = FirebaseDatabase.getInstance().getReference("foundReported")
        mStorageRef = FirebaseStorage.getInstance().getReference("uploads");

        editTextName = findViewById(R.id.editTextName)
        spinnerCategory = findViewById(R.id.spinnerCategory)
        editTextLocation = findViewById(R.id.editTextLocation)
        editTextDescription = findViewById(R.id.editTextDescription)
        imageView = findViewById(R.id.image_view)
        editTextPhone = findViewById(R.id.editTextPhone)
        sendButton = findViewById(R.id.btnSend)
        sendButton.setOnClickListener{
            addPost(userMail!!)
        }
        uploadButton = findViewById(R.id.btnAttachImage)
        uploadButton.setOnClickListener{
            attachImage()
        }



    }

    override fun onStart() {
        super.onStart()

    }
    fun addPost(userMail: String) {

        //getting the values to save
        val name = editTextName.text.toString().trim { it <= ' ' }
        val location = editTextLocation.text.toString().trim { it <= ' ' }
        val description = editTextDescription.text.toString().trim { it <= ' ' }
        val category = spinnerCategory.selectedItem.toString()
        val phoneNum = editTextPhone.text.toString().trim { it <= ' ' }

        //checking if the value is provided
        if (!TextUtils.isEmpty(name)) {

            if (imageUri == null) {
                Toast.makeText(this, "Upload an Image", Toast.LENGTH_LONG).show()
            }
            else {
                val id = databaseReportFound.push().key
                if (id != null) {
                    uploadFile(id, name, location, description, category, userMail, phoneNum)
                }
            }
        }
        else {
            //if the value is not given displaying a toast
            Toast.makeText(this, "Enter Name", Toast.LENGTH_LONG).show()
        }
    }
    fun attachImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1 && resultCode == RESULT_OK
            && data != null && data.getData() != null) {
            imageUri = data.getData();
            imageView.setImageURI(imageUri)
        }
    }

    fun getFileExtension(uri: Uri): String? {
        val cR = contentResolver
        val mime = MimeTypeMap.getSingleton()
        return mime.getExtensionFromMimeType(cR.getType(uri))
    }

    fun uploadFile(id:String, name:String, location:String, description:String, category:String, userMail:String, phoneNum:String) {
        if (imageUri !== null) {
            val fileReference = mStorageRef.child(id + "." + getFileExtension(imageUri!!));

            fileReference.putFile(imageUri!!)
                .addOnSuccessListener { _ ->
                    fileReference.downloadUrl.addOnCompleteListener() { taskSnapshot ->
                        val imageUrl = taskSnapshot.result.toString()
                        val data = ReportData(id, userMail, name, category, location, description, imageUrl, phoneNum)
                        databaseReportFound.child(id).setValue(data)

                        Toast.makeText(this, "Created a post", Toast.LENGTH_LONG).show()
                        finish()
                    }
                }
                .addOnFailureListener { exception ->
                    Toast.makeText(this, exception.message, Toast.LENGTH_LONG).show();
                }
        } else {
            Toast.makeText(this, "No file selected", Toast.LENGTH_LONG).show();
        }
    }
    companion object {
        val AUTHOR_NAME = "com.example.tesla.myhomelibrary.authorname"
        val AUTHOR_ID = "com.example.tesla.myhomelibrary.authorid"
        val USER_MAIL = "com.example.tesla.myhomelibrary.UMail"
    }
}



