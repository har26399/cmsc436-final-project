package com.example.cmsc436_final_lost_and_found

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

import java.util.ArrayList

class SearchLost : AppCompatActivity() {
    internal lateinit var buttonSearch: Button
    internal lateinit var editTextName: EditText
    internal lateinit var editTextLocation: EditText
    internal lateinit var name: TextView
    lateinit var spinnerCategory: Spinner
    internal lateinit var location: TextView
    internal lateinit var searchResult: TextView
    internal lateinit var listViewFound: ListView
    internal lateinit var items: MutableList<ReportData>
    internal lateinit var databaseItem: DatabaseReference



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.search_item)


        buttonSearch = findViewById<View>(R.id.buttonSearch) as Button
        editTextName = findViewById<View>(R.id.editTextName) as EditText
        editTextLocation = findViewById<View>(R.id.editTextLocation) as EditText
        listViewFound = findViewById<View>(R.id.listFound) as ListView
        spinnerCategory = findViewById(R.id.spinnerCategory)

        databaseItem = FirebaseDatabase.getInstance().getReference("lostReported")
        items = ArrayList()
        buttonSearch.setOnClickListener { search(editTextName.text.toString(), spinnerCategory.selectedItem.toString(), editTextLocation.text.toString()) }


        listViewFound.onItemClickListener =
            AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val item = items[i]

                val intent = Intent(applicationContext, ShowMap::class.java)

                intent.putExtra("id", item.id)
                intent.putExtra("name", item.name)
                intent.putExtra("category", item.category)
                intent.putExtra("location", item.location)
                intent.putExtra("description", item.description)
                intent.putExtra("reporter", item.reporter)
                intent.putExtra("phoneNum", item.phoneNumber)
                intent.putExtra("imageUrl", item.imageUrl)

                startActivity(intent)
            }



    }

// Todo onStart
    override fun onStart() {
        super.onStart()
    }

    fun search(n:String, c:String, l:String){
        val name = n.trim { it <= ' ' }
        val location = l.trim { it <= ' ' }
        if (name.isEmpty() || location.isEmpty() || c.isEmpty()) {
            Toast.makeText(this, "Make sure to have all the fields filled out!", Toast.LENGTH_LONG).show()
        } else {
            databaseItem.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    items.clear()
                    for (postSnapshot in dataSnapshot.children) {
                        val item = postSnapshot.getValue<ReportData>(ReportData::class.java)
                        val nameList = name.toUpperCase().split("\\s".toRegex())
                        val catList = c.toUpperCase().split("\\s".toRegex())
                        val locationList = location.toUpperCase().split("\\s".toRegex())
                        var nameContains = false
                        var catContains = false
                        var locationContains = false
                        for (namePart in nameList) {
                            if (item!!.name.toUpperCase().contains(namePart) || item!!.category.toUpperCase().equals(c.toUpperCase()) || item!!.location.toUpperCase().contains(location.toUpperCase())) {
                                nameContains = true
                            }
                        }
                        for (catPart in catList) {
                            if (item!!.category.toUpperCase().equals(catPart)) {
                                catContains = true
                            }
                        }
                        for (locationPart in locationList) {
                            if (item!!.location.toUpperCase().contains(locationPart)) {
                                locationContains = true
                            }
                        }
                        if (locationContains || nameContains || catContains) {
                            if (item != null) {
                                items.add(item)
                            }
                        }
                    }
                    val itemListAdapter = ItemList(this@SearchLost, items)
                    listViewFound.adapter = itemListAdapter
                }

                override fun onCancelled(databaseError: DatabaseError) {

                }
            })
        }
        val inputManager: InputMethodManager =getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(currentFocus!!.windowToken, InputMethodManager.SHOW_FORCED)
    }


    companion object {
        val AUTHOR_NAME = "com.example.tesla.myhomelibrary.authorname"
        val AUTHOR_ID = "com.example.tesla.myhomelibrary.authorid"
        val USER_ID = "com.example.tesla.myhomelibrary.userid"
    }
}



