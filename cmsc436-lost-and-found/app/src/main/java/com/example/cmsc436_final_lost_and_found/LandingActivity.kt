package com.example.cmsc436_final_lost_and_found

import android.content.Intent
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.Button
import android.widget.EditText
import android.widget.ListView
import android.widget.Spinner
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

import java.util.ArrayList

class LandingActivity : AppCompatActivity() {

    internal var buttonSearchLost: Button? = null
    internal var buttonSearchFound: Button? = null
    internal var buttonReportLost: Button? = null
    internal var buttonReportFound: Button? = null

    internal lateinit var listViewFound: ListView
    internal lateinit var listViewLost: ListView
    internal lateinit var dataFound: MutableList<ReportData>
    internal lateinit var dataLost: MutableList<ReportData>

    internal lateinit var databaseReportFound: DatabaseReference

    internal lateinit var databaseReportLost: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.landing)

        databaseReportFound = FirebaseDatabase.getInstance().getReference("foundReported")
        databaseReportLost = FirebaseDatabase.getInstance().getReference("lostReported")

        //UI elements
        buttonSearchLost = findViewById<View>(R.id.searchLost) as Button
        buttonSearchFound = findViewById<View>(R.id.searchFound) as Button
        buttonReportLost = findViewById<View>(R.id.reportLost) as Button
        buttonReportFound = findViewById<View>(R.id.reportFound) as Button

        buttonSearchLost!!.setOnClickListener {
            searchLost()
        }
        buttonSearchFound!!.setOnClickListener {
            searchFound()
        }
        buttonReportLost!!.setOnClickListener {
            reportLost()
        }
        buttonReportFound!!.setOnClickListener {
            reportFound()
        }

        //ListView
        listViewFound = findViewById<View>(R.id.listFound) as ListView
        listViewLost = findViewById<View>(R.id.listLost) as ListView

        dataFound = ArrayList()
        dataLost = ArrayList()

        listViewFound.onItemClickListener =
            AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val item = dataFound.filter { x -> x.reporter == intent.getStringExtra("UserEmail")}[i]

                val intent = Intent(applicationContext, ShowMap::class.java)

                intent.putExtra("id", item.id)
                intent.putExtra("name", item.name)
                intent.putExtra("category", item.category)
                intent.putExtra("location", item.location)
                intent.putExtra("description", item.description)
                intent.putExtra("reporter", item.reporter)
                intent.putExtra("phoneNum", item.phoneNumber)
                intent.putExtra("imageUrl", item.imageUrl)

                startActivity(intent)
            }

        listViewLost.onItemClickListener =
            AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val item = dataLost.filter { x -> x.reporter == intent.getStringExtra("UserEmail")}[i]

                val intent = Intent(applicationContext, ShowMap::class.java)

                intent.putExtra("id", item.id)
                intent.putExtra("name", item.name)
                intent.putExtra("category", item.category)
                intent.putExtra("location", item.location)
                intent.putExtra("description", item.description)
                intent.putExtra("reporter", item.reporter)
                intent.putExtra("phoneNum", item.phoneNumber)
                intent.putExtra("imageUrl", item.imageUrl)

                startActivity(intent)
            }
    }

// Todo onStart
    override fun onStart() {
        super.onStart()
    databaseReportFound.addValueEventListener(object : ValueEventListener {

        override fun onDataChange(dataSnapshot: DataSnapshot) {

            //clearing the previous artist list
            dataFound.clear()

            // getting found data from database

            dataSnapshot.children.mapNotNullTo(dataFound){
                it.getValue<ReportData>(ReportData::class.java)
            }

            //creating adapter using ItemList
            val itemAdapter = ItemList(this@LandingActivity, dataFound.filter { x -> x.reporter == intent.getStringExtra("UserEmail")})
            //attaching adapter to the listview
            listViewFound.adapter = itemAdapter

        }

        override fun onCancelled(databaseError: DatabaseError) {

        }
    })

    databaseReportLost.addValueEventListener(object : ValueEventListener {

        override fun onDataChange(dataSnapshot: DataSnapshot) {

            //clearing the previous artist list
            dataLost.clear()

            // getting found data from database

            dataSnapshot.children.mapNotNullTo(dataLost){
                it.getValue<ReportData>(ReportData::class.java)
            }

            //creating adapter using ItemList
            val itemAdapter = ItemList(this@LandingActivity, dataLost.filter { x -> x.reporter == intent.getStringExtra("UserEmail")})
            //attaching adapter to the listview
            listViewLost.adapter = itemAdapter

        }

        override fun onCancelled(databaseError: DatabaseError) {

        }
    })


    }


//Add put extra for type (aka search found/ report lost)
    private fun searchFound(){
        val newIntent = Intent(this@LandingActivity, SearchFound::class.java)
        startActivity(newIntent)
    }
    private fun searchLost(){
        val newIntent = Intent(this@LandingActivity, SearchLost::class.java)
        startActivity(newIntent)
    }
    private fun reportLost(){
        val newIntent = Intent(this@LandingActivity, ReportLost::class.java)
        newIntent.putExtra("UserEmail", intent.getStringExtra("UserEmail"))
        startActivity(newIntent)
    }
    private fun reportFound(){
        val newIntent = Intent(this@LandingActivity, ReportFound::class.java)
        newIntent.putExtra("UserEmail", intent.getStringExtra("UserEmail"))
        startActivity(newIntent)
    }




    companion object {
        val AUTHOR_NAME = "com.example.tesla.myhomelibrary.authorname"
        val AUTHOR_ID = "com.example.tesla.myhomelibrary.authorid"
        val USER_ID = "com.example.tesla.myhomelibrary.userid"
    }
}



