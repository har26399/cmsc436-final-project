package com.example.cmsc436_final_lost_and_found

import android.app.Activity
import android.icu.text.Collator
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView


class ItemList(private val context: Activity, internal var data: List<ReportData>) : ArrayAdapter<ReportData>(context, R.layout.layout_item_list, data) {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = context.layoutInflater
        val listViewItem = inflater.inflate(R.layout.layout_item_list, null, true)

        val ItemName = listViewItem.findViewById<View>(R.id.ItemName) as TextView
        val ItemLocation = listViewItem.findViewById<View>(R.id.ItemLocation) as TextView
        val ItemCategory = listViewItem.findViewById<View>(R.id.ItemCategory) as TextView
        val ItemDescription = listViewItem.findViewById<View>(R.id.ItemDescription) as TextView

        val item = data[position]
        ItemName.text = item.name
        ItemLocation.text = item.location
        ItemDescription.text = item.description
        ItemCategory.text = item.category


        return listViewItem
    }
}